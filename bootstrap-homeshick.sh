#!/bin/bash -ex

# Paste this into ssh
# curl -sL https://gist.github.com/zundr/8080633/raw/bootstrap_homeshick.sh | tar -xzO | /bin/bash -ex
# When forking, you can get the URL from the raw (<>) button.

### Set some command variables depending on whether we are root or not ###
# This assumes you use a debian derivate, replace with yum, pacman etc.
aptget='sudo apt-get'
chsh='sudo chsh'
if [ `whoami` = 'root' ]; then
	aptget='apt-get'
	chsh='chsh'
fi

### Install git and some other tools we'd like to use ###
$aptget update
$aptget install -y zsh-beta tmux vim git awesome awesome-extra luajit luakit

### Install homeshick ###
homeshick_repo=$HOME/.homesick/repos/homeshick
if [ -d $homeshick_repo ]; then
  (cd $homeshick_repo && git pull)
else
  git clone git://github.com/andsens/homeshick.git $homeshick_repo
fi
source $homeshick_repo/homeshick.sh

### Clone main repos ###
homeshick --batch clone ssh://git@bitbucket.org/zundr/castle-zundr-dotfiles
homeshick --batch clone ssh://git@bitbucket.org/zundr/castle-zundr-vimfiles
(homeshick cd castle-zundr-vimfiles && git checkout neobundle-reorg)

### Clone public repos ###
homeshick clone --batch sorin-ionescu/prezto
homeshick clone --batch Silox/tmux-powerline

### Link it all to $HOME ###
homeshick link --force

### Set default shell to your favorite shell ###
if [ -f /bin/zsh ]; then
  $chsh --shell /bin/zsh `whoami`
  echo "Log in again to start your proper shell"
fi

